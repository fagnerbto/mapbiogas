package org.hidroinformatica.controller;

import java.util.Collections;
import java.util.List;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.hidroinformatica.components.LoggedUser;
import org.hidroinformatica.dao.interfaces.CategoriaDeUnidadeDAO;
import org.hidroinformatica.dao.interfaces.EscalaDAO;
import org.hidroinformatica.dao.interfaces.EstadoDAO;
import org.hidroinformatica.dao.interfaces.FonteDoSubstratoDAO;
import org.hidroinformatica.dao.interfaces.MunicipioDAO;
import org.hidroinformatica.dao.interfaces.SituacaoDaPlantaDAO;
import org.hidroinformatica.dao.interfaces.UnidadeDAO;
import org.hidroinformatica.mapbiogas.model.CategoriaDeUnidade;
import org.hidroinformatica.mapbiogas.model.Escala;
import org.hidroinformatica.mapbiogas.model.Estado;
import org.hidroinformatica.mapbiogas.model.FonteDoSubstrato;
import org.hidroinformatica.mapbiogas.model.Municipio;
import org.hidroinformatica.mapbiogas.model.SituacaoDaPlanta;
import org.hidroinformatica.mapbiogas.model.Unidade;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.simplemail.Mailer;
import br.com.caelum.vraptor.view.Results;

@Resource
public class UnidadeController {
	private Result result;
	private EstadoDAO estadoDAO;
	private CategoriaDeUnidadeDAO categoriaDeUnidadeDAO;
	private SituacaoDaPlantaDAO situacaoDaPlantaDAO;
	private UnidadeDAO unidadeDAO;
	private EscalaDAO escalaDAO;
	private MunicipioDAO municipioDAO;
	private Estado estado;
	private Municipio municipio;
	private CategoriaDeUnidade categoriaDeUnidade;
	private SituacaoDaPlanta situacaoDaPlanta;
	private Unidade unidade;
	private Escala escala;
	private FonteDoSubstrato fonteDoSubstrato;
	private FonteDoSubstratoDAO fonteDoSubstratoDAO;
	private Mailer mailer;
	private LoggedUser loggedUser;
	
	public UnidadeController(Result result
							,EstadoDAO estadoDAO
							,Estado estado							
							,MunicipioDAO municipioDAO
							,Municipio municipio
							,CategoriaDeUnidadeDAO categoriaDeUnidadeDAO
							,CategoriaDeUnidade categoriaDeUnidade
							,SituacaoDaPlantaDAO situacaoDaPlantaDAO
							,SituacaoDaPlanta situacaoDaPlanta
							,UnidadeDAO unidadeDAO
							,Unidade unidade
							,EscalaDAO escalaDAO
							,Escala escala
							,FonteDoSubstratoDAO fonteDoSubstratoDAO
							,FonteDoSubstrato fonteDoSubstrato, Mailer mailer,LoggedUser loggedUser) {
		
		this.result = result;
		this.estadoDAO = estadoDAO;
		this.estado = estado;
		this.municipioDAO = municipioDAO;
		this.municipio = municipio;
		this.categoriaDeUnidadeDAO = categoriaDeUnidadeDAO;
		this.categoriaDeUnidade = categoriaDeUnidade;
		this.situacaoDaPlantaDAO = situacaoDaPlantaDAO;
		this.situacaoDaPlanta = situacaoDaPlanta;
		this.unidadeDAO = unidadeDAO;
		this.unidade = unidade;
		this.escalaDAO = escalaDAO;
		this.escala = escala;
		this.fonteDoSubstratoDAO = fonteDoSubstratoDAO;
		this.fonteDoSubstrato = fonteDoSubstrato;
		this.mailer = mailer;
		this.loggedUser = loggedUser;
	}
	// unidadeForm
	@Path("/unidade/edit/{id}")
	public void edit(Long id) {
		if (!loggedUser.isLogged()) {
			result.redirectTo(LoginController.class).form();
		} else {
			if (id != null) {
				unidade = unidadeDAO.buscarPorId(id);
				if (unidade != null) {
					result.include("unidade", unidade);

					unidade.getMunicipio();
					unidade.getMunicipio().getEstado();

					unidade.getFonteDoSubstrato().getCategoriaDeUnidade();
					result.include("fontesDeSubstrato", fonteDoSubstratoDAO
							.findByCategoriaDeUnidade(unidade
									.getFonteDoSubstrato()
									.getCategoriaDeUnidade().getId()));

					unidade.getMunicipio().getEstado();
					result.include("municipios", municipioDAO.listar());

					List<Estado> estados = estadoDAO.listar();
					result.include("estados", estados);

					List<SituacaoDaPlanta> situacoes = situacaoDaPlantaDAO
							.listar();
					result.include("situacoes", situacoes);

					List<CategoriaDeUnidade> categorias = categoriaDeUnidadeDAO
							.listar();
					result.include("categorias", categorias);

				} else {
					this.form();
				}
			}
		}
	}
	
	@Path({"/admin/form","/admin/form/"})
	public void form(){
		if(!loggedUser.isLogged()){
			result.redirectTo(LoginController.class).form();
		}else{

			List<Estado> estados = estadoDAO.listar();
			result.include("estados",estados);
			
			List<SituacaoDaPlanta> situacoes = situacaoDaPlantaDAO.listar(); 
			result.include("situacoes",situacoes);
			
			List<CategoriaDeUnidade> categorias = categoriaDeUnidadeDAO.listar();
			result.include("categorias",categorias);		
		}				
	}
	
	public void salvar(Unidade unidade, Long categoriaId ){		
		if(!loggedUser.isLogged()){
			result.redirectTo(LoginController.class).form();
		}else{		
			Long producaoDiaria = unidade.getProducaoDiaria();		
			
			if( (producaoDiaria > 0) && (producaoDiaria <= 1250)){
				unidade.setEscala(escalaDAO.getByPorte(1L));
			}else if((producaoDiaria >= 1251) && (producaoDiaria <= 2500) ){
				unidade.setEscala(escalaDAO.getByPorte(2L));
			}else if((producaoDiaria >= 2501) && (producaoDiaria <= 8500)){
				unidade.setEscala(escalaDAO.getByPorte(3L));
			}else if((producaoDiaria >= 8501) && (producaoDiaria <= 12500)){
				unidade.setEscala(escalaDAO.getByPorte(4L));
			}else if((producaoDiaria >= 12501) && (producaoDiaria <= 85500)){
				unidade.setEscala(escalaDAO.getByPorte(5L));
			}else if((producaoDiaria >= 85501) && (producaoDiaria <= 350000)){
				unidade.setEscala(escalaDAO.getByPorte(6L));
			}else{
				unidade.setEscala(escalaDAO.getByPorte(7L));
			}
			
			if(unidade.getId() == null){
				unidadeDAO.adicionar(unidade);
			}else{
				unidadeDAO.alterar(unidade);			
			}		
		result.redirectTo(this).lista();
		}
	}

	@Path({"/admin/lista","/admin/lista/","/admin","/admin/"})
	public void lista(){
		if(!loggedUser.isLogged()){
			result.redirectTo(LoginController.class).form();
		}else{		
			List<Unidade> lista = unidadeDAO.listar();
			result.include("lista", lista);
		}
	}
	
	@Path({"/unidade/lista/json","/unidade/lista/json/"})
	public void listaUnidadesJson(){
		List<Unidade> lista = unidadeDAO.listar();
		result.use(Results.json()).withoutRoot().from(lista).include("situacaoDaPlanta").include("escala").include("fonteDoSubstrato").include("municipio").serialize();
	}
	
	@Path({"/map","/map/","/"})
	public void map(){
		List<Estado> estados = estadoDAO.listar();
		result.include("estados",estados);
		
		List<SituacaoDaPlanta> situacoes = situacaoDaPlantaDAO.listar(); 
		result.include("situacoes",situacoes);
		
		List<CategoriaDeUnidade> categorias = categoriaDeUnidadeDAO.listar();
		result.include("categorias",categorias);
		
		List<FonteDoSubstrato> fontes = fonteDoSubstratoDAO.listar();
		result.include("fontes",fontes);
		
		List<Escala> escalas = escalaDAO.listar();
		Collections.sort(escalas);		
		result.include("escalas",escalas);
		
		List<Unidade> lista = unidadeDAO.listar();
		result.include(lista);
	}
	
	@Get({"/testa","/testa/"})	
	public void mapWithFilter(Long estadoId
							, Long municipioId
							, Long situacaoDaPlantaId
							, Long categoriaId
							, Long fonteDoSubstratoId
							, Long escalaId
							, Long porteS
							, Boolean energiaTermica
							, Boolean energiaEletrica
							, Boolean energiaMecanica
							, Boolean bioMetano
							, Mailer mailer
							){
		
		estado = estadoDAO.buscarPorId(estadoId);
		municipio = municipioDAO.buscarPorId(municipioId);
		fonteDoSubstrato = fonteDoSubstratoDAO.buscarPorId(fonteDoSubstratoId);		
		situacaoDaPlanta = situacaoDaPlantaDAO.buscarPorId(situacaoDaPlantaId);
		
		List<Unidade> lista =  unidadeDAO.unidadesPorFiltro(estado
				, municipio
				, situacaoDaPlanta
				, categoriaDeUnidade
				, fonteDoSubstrato
				, escala
				, energiaTermica
				, energiaEletrica
				, energiaMecanica
				, bioMetano);
		
		result.use(Results.json()).withoutRoot().from(lista).recursive().serialize();
	}
	
	
	
	@Get({"/getData","/getData/"})	
	public void getFilteredDataForMap(Long estadoId
							, Long municipioId
							, Long situacaoDaPlantaId
							, Long categoriaId
							, Long fonteDoSubstratoId
							, Long escalaId
							, Long porteS
							, Boolean energiaTermica
							, Boolean energiaEletrica
							, Boolean energiaMecanica
							, Boolean bioMetano		
							){
		
		estado = estadoDAO.buscarPorId(estadoId);
		municipio = municipioDAO.buscarPorId(municipioId);
		escala = escalaDAO.buscarPorId(escalaId);
		fonteDoSubstrato = fonteDoSubstratoDAO.buscarPorId(fonteDoSubstratoId);		
		situacaoDaPlanta = situacaoDaPlantaDAO.buscarPorId(situacaoDaPlantaId);
		
		List<Unidade> lista =  unidadeDAO.unidadesPorFiltro(estado
				, municipio
				, situacaoDaPlanta
				, categoriaDeUnidade
				, fonteDoSubstrato
				, escala
				, energiaTermica
				, energiaEletrica
				, energiaMecanica
				, bioMetano);
		
		result.use(Results.json()).withoutRoot().from(lista).recursive().serialize();
	
	}

	@Path("/unidade/exclude/{id}")
	public void excluir(Long id){
		if(!loggedUser.isLogged()){
			result.redirectTo(LoginController.class).form();
		}else{
			try {
				unidadeDAO.excluir(unidadeDAO.buscarPorId(id));	
			} catch (Exception e) {
				if(e.getCause() !=null){
					System.out.println(e.getCause());
				}else{
					System.out.println("Erro ao excluir!");
				}
			}finally{
				result.redirectTo(this).lista();
			}
		}
	};
	
	@Path({"/contato", "/contato/"})
	public void contato(String nome, String email, String mensagem){

		try {
			Email simpleEmail = new SimpleEmail();
			simpleEmail.setSubject("Um visitante do sitema MapBiogas entrou em contato");
			simpleEmail.addTo("cadastro.biogas@cibiogas.org");
			simpleEmail.setMsg("Seguem os dados: \n"
					+ "Nome: " + nome
					+ "\nContato: " + email
					+ "\nMensagem: "+ mensagem);

			simpleEmail.setCharset("utf-8");
			this.mailer.send(simpleEmail);
			
		} catch (EmailException e) {
			e.printStackTrace();
		}

	result.include("mensagem", "Email enviado com sucesso!");
	result.redirectTo(this).map();
	}
}