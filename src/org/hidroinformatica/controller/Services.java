package org.hidroinformatica.controller;

import java.util.List;

import org.hidroinformatica.dao.interfaces.EstadoDAO;
import org.hidroinformatica.dao.interfaces.FonteDoSubstratoDAO;
import org.hidroinformatica.dao.interfaces.MunicipioDAO;
import org.hidroinformatica.mapbiogas.model.FonteDoSubstrato;
import org.hidroinformatica.mapbiogas.model.Municipio;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;

@Resource
public class Services {
	private Result result;
	private MunicipioDAO municipioDAO;
	private FonteDoSubstratoDAO fonteDoSubstratoDAO;

	public Services(Result result, EstadoDAO estadoDAO, MunicipioDAO municipioDAO, FonteDoSubstratoDAO fonteDoSubstratoDAO ) {
		this.result = result;
		this.municipioDAO = municipioDAO;
		this.fonteDoSubstratoDAO = fonteDoSubstratoDAO;
	}
	
	
	@Get("/municipios/estado/{estadoId}")
	public void getMunicipios(Long estadoId){
		List<Municipio> lista =  municipioDAO.municipiosPorEstado(estadoId);
		result.use(Results.json()).withoutRoot().from(lista).serialize();  
		
	}
	@Get("/fonteDoSubstratoPorCategoriaDeUnidade/{id}")
	public void getFontesDeSubstrato(Long id){
		List<FonteDoSubstrato> lista = fonteDoSubstratoDAO.findByCategoriaDeUnidade(id);
		result.use(Results.json()).withoutRoot().from(lista).serialize();		
	}
}
