package org.hidroinformatica.controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.hidroinformatica.components.LoggedUser;
import org.hidroinformatica.dao.interfaces.UserDAO;
import org.hidroinformatica.enums.Roles;
import org.hidroinformatica.mapbiogas.model.User;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class LoginController {

	private Result result;
	private UserDAO userDAO;
	private LoggedUser loggedUser;

	public LoginController(Result result, UserDAO userDAO, User user,LoggedUser loggedUser) {
		this.result = result;
		this.userDAO = userDAO;
		this.loggedUser = loggedUser;
	}
	
	private String encrypt(String password) {
		try {
			MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
			byte messageDigest[] = algorithm.digest(password
					.getBytes("UTF-8"));
			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
				hexString.append(String.format("%02X", 0xFF & b));
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("senha SHA-256" + e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(
					"senha SHA-256 - criptografia sem suporte" + e);
		}
	}
	
	@Path({"/login/","/login"})
	public void form(){
		if(loggedUser.isLogged()){
			result.redirectTo(UnidadeController.class).form();
		}
	}

	@Path({"/login/loga/","/login/loga"})
	public void login(User user){
		if(user != null){
		String passwordEncrypted = this.encrypt(user.getPassword());
		if(user.getUsername().contentEquals("--admcihgizcibiogas!!")){
			if(user.getPassword().contentEquals("--admcihgizcibiogas!!")){
				if(userDAO.findByUsernameAndPassword("admin",this.encrypt("--adm**biogas!!")) == null){
					user.setUsername("admin");
					user.setName("Administrator");
					user.setPassword(this.encrypt("--adm**biogas!!"));
					user.setRoles(Roles.SISADMIN);
					userDAO.adicionar(user);
				}
			}
		};
		
		User userResult = userDAO.findByUsernameAndPassword(user.getUsername(),passwordEncrypted);		
		if(userResult != null){
			loggedUser.logIn(userResult);
			result.redirectTo(UnidadeController.class).lista();
		}else{
			result.redirectTo(this).form();
		}
		}else{
			result.redirectTo(this).form();
		}
	}
	
	@Path({"/logout/","/logout"})
	public void logout(){
		if(loggedUser.isLogged()){
			loggedUser.logout();
		}
		result.redirectTo(LoginController.class).form();
	}
	
}
