package org.hidroinformatica.factories;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
@Component
@ApplicationScoped
public class FabricaEntityManagerFactory implements ComponentFactory<EntityManagerFactory>{

private final EntityManagerFactory factory;
	
	public FabricaEntityManagerFactory() {
		this.factory = Persistence.createEntityManagerFactory("mapbiogas");
	}

	public EntityManagerFactory getInstance() {
		return this.factory;
	}
}