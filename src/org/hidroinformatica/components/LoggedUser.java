package org.hidroinformatica.components;

import org.hidroinformatica.mapbiogas.model.User;


import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

@Component
@SessionScoped
public class LoggedUser {

	private User user;
	
	public void logIn(User user) {
		this.setUser(user);
	}
	
	public boolean isLogged() {
			return this.getUser() != null;
	}
	
	public User getLoggedUser() {
		return getUser();
	}
	
	public void logout() {
		this.setUser(null);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}