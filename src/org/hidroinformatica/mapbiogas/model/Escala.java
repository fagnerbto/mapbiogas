package org.hidroinformatica.mapbiogas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Escala implements Serializable, Comparable<Escala> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4297333466017838941L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;	
	private Long valor; // valor de referencia para o MENOR QUE
	private Long intervalo; // 1, 2 ou 3 (Pequeno, Médio, Grande)
	private String legenda; //
	/* < 1.250 m3/dia - Porte 1 
 	 * 1251 a 2.500 m3/dia - Porte 2 
 	 * 2501 a 8.500 m3/dia - Porte 3 
 	 * 8501 a 12.500 m3/dia - Porte 4 
 	 * 12501 a 85.500 m3/dia - Porte 5 
 	 * 85501 a 350.000 m3/dia - Porte 6 
	 * > 350.000 m3/dia - Porte 7 
	 */
	private Long porte; /* Porte 1 , Porte 2, Porte 3 */
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getValor() {
		return valor;
	}
	public void setValor(Long valor) {
		this.valor = valor;
	}
	public Long getIntervalo() {
		return intervalo;
	}
	public void setIntervalo(Long intervalo) {
		this.intervalo = intervalo;
	}
	public String getLegenda() {
		return legenda;
	}
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}
	
	public Long getPorte() {
		return porte;
	}
	public void setPorte(Long porte) {
		this.porte = porte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Escala other = (Escala) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public int compareTo(Escala outraEscala) {
		 if (this.id < outraEscala.id) {
	            return -1;
	        }
	        if (this.id > outraEscala.id) {
	            return 1;
	        }
	        return 0;
	}	
}
