package org.hidroinformatica.mapbiogas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Municipio implements Serializable{
	
	private static final long serialVersionUID = -1221925876197053144L;
	
	@Id
	private Long id;
	private String nome;
	private String uf;
	
	@ManyToOne
	private Estado estado;
	
	public Municipio() {
		
	}
	
	public Municipio(int id, int idEstado, int ibgeCode, String nome, String uf ){
		this.id = (long) id;
		this.nome = nome;
		this.uf = uf;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
}
