package org.hidroinformatica.mapbiogas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Unidade implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2826009945715327578L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private Boolean biogasParaEnergiaTermica = false;
	private Boolean biogasParaEnergiaEletrica = false;
	private Boolean biogasParaEnergiaMecanica = false;
	private Boolean biogasParaBiometano = false;

	private Long codigo;
	
	private Double latitude;
	private Double longitude;

	private Long producaoDiaria;
	
	private Boolean valorEstimado = false;

	@ManyToOne
	private SituacaoDaPlanta situacaoDaPlanta;

	@ManyToOne
	private Escala escala;
	
	@ManyToOne
	private FonteDoSubstrato fonteDoSubstrato;

	@ManyToOne
	private Municipio municipio;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBiogasParaEnergiaTermica() {
		return biogasParaEnergiaTermica;
	}

	public void setBiogasParaEnergiaTermica(Boolean biogasParaEnergiaTermica) {
		this.biogasParaEnergiaTermica = biogasParaEnergiaTermica;
	}

	public Boolean getBiogasParaEnergiaEletrica() {
		return biogasParaEnergiaEletrica;
	}

	public void setBiogasParaEnergiaEletrica(Boolean biogasParaEnergiaEletrica) {
		this.biogasParaEnergiaEletrica = biogasParaEnergiaEletrica;
	}

	public Boolean getBiogasParaEnergiaMecanica() {
		return biogasParaEnergiaMecanica;
	}

	public void setBiogasParaEnergiaMecanica(Boolean biogasParaEnergiaMecanica) {
		this.biogasParaEnergiaMecanica = biogasParaEnergiaMecanica;
	}

	public Boolean getBiogasParaBiometano() {
		return biogasParaBiometano;
	}

	public void setBiogasParaBiometano(Boolean biogasParaBiometano) {
		this.biogasParaBiometano = biogasParaBiometano;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public SituacaoDaPlanta getSituacaoDaPlanta() {
		return situacaoDaPlanta;
	}

	public void setSituacaoDaPlanta(SituacaoDaPlanta situacaoDaPlanta) {
		this.situacaoDaPlanta = situacaoDaPlanta;
	}

	public Escala getEscala() {
		return escala;
	}

	public void setEscala(Escala escala) {
		this.escala = escala;
	}

	public FonteDoSubstrato getFonteDoSubstrato() {
		return fonteDoSubstrato;
	}

	public void setFonteDoSubstrato(FonteDoSubstrato fonteDoSubstrato) {
		this.fonteDoSubstrato = fonteDoSubstrato;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unidade other = (Unidade) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean getValorEstimado() {
		return valorEstimado;
	}

	public void setValorEstimado(Boolean valorEstimado) {
		this.valorEstimado = valorEstimado;
	}

	public Long getProducaoDiaria() {
		return producaoDiaria;
	}

	public void setProducaoDiaria(Long producaoDiaria) {
		this.producaoDiaria = producaoDiaria;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	
}
		