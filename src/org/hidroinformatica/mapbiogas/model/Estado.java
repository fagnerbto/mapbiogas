package org.hidroinformatica.mapbiogas.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Estado implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3317027512794404277L;

	public Estado() {
		// TODO Auto-generated constructor stub
	}
	
	public Estado(int id, int ibgeCode, String nome, String sigla, String regiao) {
		this.id = (long) id;
		this.ibgeCode = (long) ibgeCode;
		this.nome = nome;
		this.sigla = sigla;
		this.regiao = regiao;
	}

	@Id
	private Long id;
	private Long ibgeCode;	
	private String nome;
	private String sigla;
	private String regiao;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIbgeCode() {
		return ibgeCode;
	}
	public void setIbgeCode(Long ibgeCode) {
		this.ibgeCode = ibgeCode;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getRegiao() {
		return regiao;
	}
	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
