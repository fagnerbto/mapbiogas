package org.hidroinformatica.mapbiogas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class FonteDoSubstrato implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1409633054806578117L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column
	private String nome;
	
	@ManyToOne
	private CategoriaDeUnidade categoriaDeUnidade;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public CategoriaDeUnidade getCategoriaDeUnidade() {
		return categoriaDeUnidade;
	}
	public void setCategoriaDeUnidade(CategoriaDeUnidade categoriaDeUnidade) {
		this.categoriaDeUnidade = categoriaDeUnidade;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FonteDoSubstrato other = (FonteDoSubstrato) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
