package org.hidroinformatica.dao.impl;

import javax.persistence.EntityManager;

import org.hidroinformatica.mapbiogas.model.CategoriaDeUnidade;
import org.hidroinformatica.dao.interfaces.CategoriaDeUnidadeDAO;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class CategoriaDeUnidadeDAOImpl extends DAOImpl<CategoriaDeUnidade> implements CategoriaDeUnidadeDAO{
	
	public EntityManager em;
	public CategoriaDeUnidadeDAOImpl(EntityManager em) {
		super(em);
		this.em = em;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return CategoriaDeUnidade.class;
	}

	public CategoriaDeUnidade findByName(String nome) {
		return (CategoriaDeUnidade) em.createQuery("from CategoriaDeUnidade Where nome = :nome").setParameter("nome", nome).getSingleResult();
	}

}
