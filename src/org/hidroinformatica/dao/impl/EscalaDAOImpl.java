package org.hidroinformatica.dao.impl;

import javax.persistence.EntityManager;

import org.hidroinformatica.mapbiogas.model.Escala;
import org.hidroinformatica.dao.interfaces.EscalaDAO;

import br.com.caelum.vraptor.ioc.Component;
@Component
public class EscalaDAOImpl extends DAOImpl<Escala> implements EscalaDAO{

	private EntityManager em;

	public EscalaDAOImpl(EntityManager em) {
		super(em);
		this.em = em;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return Escala.class;
	}

	public Escala getByPorte(Long porte) {
		return (Escala) em.createQuery("from Escala where porte = :porte").setParameter("porte", porte).getSingleResult();
	}
}
