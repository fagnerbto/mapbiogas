package org.hidroinformatica.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hidroinformatica.mapbiogas.model.User;
import org.hidroinformatica.dao.interfaces.UserDAO;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class UserDAOImpl extends DAOImpl<User> implements UserDAO{

	public UserDAOImpl(EntityManager em) {
		super(em);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return User.class;
	}

	public User findByUserName(String username) {
		User user;
		
		Query query = super.getEm().createQuery("select u from User u where u.username = :username");
		query.setParameter("username", username);
		
		user = (User) query.getSingleResult();

		return user;
	}

	public User findByUsernameAndPassword(String username, String password) {
		Query query = super.getEm().createQuery(
				"select u from User u where u.username = '" + username
						+ "' and u.password = '" + password + "'");
		if (!query.getResultList().isEmpty()) {
			return (User) query.getSingleResult();
		}
		return null;
	}
}