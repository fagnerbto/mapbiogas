package org.hidroinformatica.dao.impl;

import javax.persistence.EntityManager;

import org.hidroinformatica.mapbiogas.model.Estado;
import org.hidroinformatica.dao.interfaces.EstadoDAO;
import br.com.caelum.vraptor.ioc.Component;

@Component
public class EstadoDAOImpl extends DAOImpl<Estado> implements EstadoDAO {

	private EntityManager em;

	public EstadoDAOImpl(EntityManager em) {
		super(em);
		this.em = em;
	}

	public Estado findByName(String nome) {
		return (Estado) em.createQuery("from Estado where nome = :nome").setParameter("nome", nome).getSingleResult();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return Estado.class;
	}

}
