package org.hidroinformatica.dao.impl;

import javax.persistence.EntityManager;

import org.hidroinformatica.mapbiogas.model.SituacaoDaPlanta;
import org.hidroinformatica.dao.interfaces.SituacaoDaPlantaDAO;

import br.com.caelum.vraptor.ioc.Component;
@Component
public class SituacaoDaPlantaDAOImpl extends DAOImpl<SituacaoDaPlanta> implements SituacaoDaPlantaDAO {

	private EntityManager em;

	public SituacaoDaPlantaDAOImpl(EntityManager em) {
		super(em);
		this.em = em;
	}

	public SituacaoDaPlanta findByName(String nome) {
		return (SituacaoDaPlanta) em.createQuery("from SituacaoDaPlanta where nome = :nome")
				.setParameter("nome", nome);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return SituacaoDaPlanta.class;
	}

}
