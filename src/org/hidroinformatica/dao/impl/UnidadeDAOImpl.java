package org.hidroinformatica.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hidroinformatica.mapbiogas.model.CategoriaDeUnidade;
import org.hidroinformatica.mapbiogas.model.Escala;
import org.hidroinformatica.mapbiogas.model.Estado;
import org.hidroinformatica.mapbiogas.model.FonteDoSubstrato;
import org.hidroinformatica.mapbiogas.model.Municipio;
import org.hidroinformatica.mapbiogas.model.SituacaoDaPlanta;
import org.hidroinformatica.mapbiogas.model.Unidade;
import org.hidroinformatica.dao.interfaces.UnidadeDAO;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class UnidadeDAOImpl extends DAOImpl<Unidade> implements UnidadeDAO{

	private EntityManager em;

	public UnidadeDAOImpl(EntityManager em) {
		super(em);
		this.em = em;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {		
		return Unidade.class;
	}

	@SuppressWarnings("unchecked")
	public List<Unidade> unidadesPorFiltro	(Estado estado
											,Municipio municipio											
											,SituacaoDaPlanta situacaoDaPlanta
											,CategoriaDeUnidade categoriaDeUnidade
											,FonteDoSubstrato fonteDoSubstrato
											,Escala escala
											,Boolean energiaTermica
											,Boolean energiaEletrica
											,Boolean energiaMecanica
											,Boolean bioMetano
											){

		Session session = 	em.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Unidade.class);

		String query = "Esta busca envolverá ";
		
		if(energiaTermica == true){
			criteria.add(Restrictions.eq("biogasParaEnergiaTermica", energiaTermica));
			query += "Energia Térmica ... ";
		}
		
		if(energiaMecanica== true){
			criteria.add(Restrictions.eq("biogasParaEnergiaMecanica", energiaMecanica));
			query += "Energia Mecânica ... ";
		}
		
		if(energiaEletrica == true){
			criteria.add(Restrictions.eq("biogasParaEnergiaEletrica", energiaEletrica));
			query += "Energia Elétrica ... ";
		}
		
		if(bioMetano == true){
			criteria.add(Restrictions.eq("biogasParaBiometano", bioMetano));
			query += "Energia Biometano ... ";		
		}
		
		
//		if(porte !=null){
//			criteria.createAlias("escala", "e");
//			criteria.add(Restrictions.eq("escala.intervalo", porte));
//		}
		
		if(municipio != null){
			criteria.add(Restrictions.eq("municipio", municipio));
		}else{
			if(estado !=null){
				criteria.createAlias("municipio", "m");
				criteria.add(Restrictions.eq("m.estado", estado));
			}
		}

		if(fonteDoSubstrato != null){
			criteria.add(Restrictions.eq("fonteDoSubstrato",fonteDoSubstrato));
		}

		if(situacaoDaPlanta != null){
			criteria.add(Restrictions.eq("situacaoDaPlanta", situacaoDaPlanta));
		}
		
		if(escala != null){
			criteria.add(Restrictions.eq("escala",escala));
		}
		List<Unidade> lista = criteria.list();
		return lista;
	}
}
