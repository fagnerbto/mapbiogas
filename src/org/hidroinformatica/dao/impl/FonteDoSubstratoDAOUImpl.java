package org.hidroinformatica.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.hidroinformatica.mapbiogas.model.FonteDoSubstrato;
import org.hidroinformatica.dao.interfaces.FonteDoSubstratoDAO;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class FonteDoSubstratoDAOUImpl extends DAOImpl<FonteDoSubstrato> implements FonteDoSubstratoDAO {

	public EntityManager em;

	public FonteDoSubstratoDAOUImpl(EntityManager em) {
		super(em);
		this.em = em;
		
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return FonteDoSubstrato.class;
	}
	
	public FonteDoSubstrato findByName(String nome) {
		return (FonteDoSubstrato) em.createQuery("from FonteDoSubstrato where nome = :nome").setParameter("nome", nome).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<FonteDoSubstrato> findByCategoriaDeUnidade(Long categoriaDeUnidade_id) {
		return (List<FonteDoSubstrato>) em.createQuery("from FonteDoSubstrato where categoriadeunidade_id = :categoriaDeUnidade_id").setParameter("categoriaDeUnidade_id", categoriaDeUnidade_id).getResultList();
	}
}
