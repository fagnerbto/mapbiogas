package org.hidroinformatica.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.hidroinformatica.mapbiogas.model.Municipio;
import org.hidroinformatica.dao.interfaces.MunicipioDAO;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class MunicipioDAOImpl extends DAOImpl<Municipio> implements MunicipioDAO {

	private EntityManager em;

	public MunicipioDAOImpl(EntityManager em) {
		super(em);
		this.em = em;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getClazz() {
		return Municipio.class;
	}

	public Municipio findByName(String nome) {
		return (Municipio) em.createQuery("from Municipio Where nome = :nome").setParameter("nome", nome).getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Municipio> municipiosPorEstado(Long idEstado) {
		return (List<Municipio>) em.createQuery("from Municipio where idEstado = :idEstado").setParameter("idEstado", idEstado).getResultList();
	}
}
