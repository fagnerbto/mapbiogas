package org.hidroinformatica.dao.interfaces;

import java.util.List;

import org.hidroinformatica.mapbiogas.model.FonteDoSubstrato;

public interface FonteDoSubstratoDAO extends DAO<FonteDoSubstrato>{

	public FonteDoSubstrato findByName(String nome);
	public List<FonteDoSubstrato> findByCategoriaDeUnidade(Long id);
}