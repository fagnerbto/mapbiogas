package org.hidroinformatica.dao.interfaces;

import org.hidroinformatica.mapbiogas.model.User;

public interface UserDAO extends DAO<User>{
	User findByUserName(String username);
	User findByUsernameAndPassword(String username, String password);
}