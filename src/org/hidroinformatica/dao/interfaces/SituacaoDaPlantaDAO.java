package org.hidroinformatica.dao.interfaces;

import org.hidroinformatica.mapbiogas.model.SituacaoDaPlanta;

public interface SituacaoDaPlantaDAO extends DAO<SituacaoDaPlanta>{
	 public SituacaoDaPlanta findByName(String nome);
}