package org.hidroinformatica.dao.interfaces;

import java.util.List;

import org.hidroinformatica.mapbiogas.model.Municipio;

public interface MunicipioDAO extends DAO<Municipio>{
	public Municipio findByName(String nome);
	public List<Municipio> municipiosPorEstado(Long id);
}
