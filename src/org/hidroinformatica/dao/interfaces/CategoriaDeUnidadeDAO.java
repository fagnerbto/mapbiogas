package org.hidroinformatica.dao.interfaces;

import org.hidroinformatica.mapbiogas.model.CategoriaDeUnidade;

public interface CategoriaDeUnidadeDAO extends DAO<CategoriaDeUnidade> {

	public CategoriaDeUnidade findByName(String nome);
}
