package org.hidroinformatica.dao.interfaces;

import java.util.List;

public interface DAO<T> {
	
	public List<T> listar();

	public void adicionar(T objeto);

	public void excluir(T objeto);
	
	public void alterar(T objeto);

	public T buscarPorId(Long id);
}
