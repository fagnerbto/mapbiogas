package org.hidroinformatica.dao.interfaces;

import org.hidroinformatica.mapbiogas.model.Escala;

public interface EscalaDAO extends DAO<Escala>{
	public Escala getByPorte(Long porte);
}