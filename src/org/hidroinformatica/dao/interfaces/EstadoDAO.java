package org.hidroinformatica.dao.interfaces;

import org.hidroinformatica.mapbiogas.model.Estado;

public interface EstadoDAO extends DAO<Estado>{
	public Estado findByName(String nome);
	
}
