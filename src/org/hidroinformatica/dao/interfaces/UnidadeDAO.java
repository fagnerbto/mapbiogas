package org.hidroinformatica.dao.interfaces;

import java.util.List;

import org.hidroinformatica.mapbiogas.model.CategoriaDeUnidade;
import org.hidroinformatica.mapbiogas.model.Escala;
import org.hidroinformatica.mapbiogas.model.Estado;
import org.hidroinformatica.mapbiogas.model.FonteDoSubstrato;
import org.hidroinformatica.mapbiogas.model.Municipio;
import org.hidroinformatica.mapbiogas.model.SituacaoDaPlanta;
import org.hidroinformatica.mapbiogas.model.Unidade;

public interface UnidadeDAO extends DAO<Unidade> {
//	public List<Municipio> municipiosPorEstado(Long id);
public List<Unidade> unidadesPorFiltro(Estado estado
		,Municipio municipio											
		,SituacaoDaPlanta situacaoDaPlanta
		,CategoriaDeUnidade categoriaDeUnidade
		,FonteDoSubstrato fonteDoSubstrato
		,Escala escala
		,Boolean energiaTermica
		,Boolean energiaEletrica
		,Boolean energiaMecanica
		,Boolean bioMetano
		);
}