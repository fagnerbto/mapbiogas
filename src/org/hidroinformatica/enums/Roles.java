package org.hidroinformatica.enums;

public enum Roles {
	SISADMIN("Sistem Administrator"); 

	private final String name;
	
	Roles (String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
