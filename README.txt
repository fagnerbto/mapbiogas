Neste projeto houve uma complicação em relação ao subdomínio.
desta forma a aplicação tem nas suas páginas JSP uma linha definindo 
uma variável $project
na qual o value é localhost:NumeroDaPorta ou mapbiogas.cibiogas.org  (sempre 
sem o "/" no final
Por exemplo:

<c:set var="project" value="http://URL.NA.QUAL.O.PROJETO.RODA" />

Para resolver a mesma questão da parte javascript é criada uma variável chamada
context que recebe o valor de $project.

Por exemplo:

<script type="text/javascript">
	var context = '${project}';
</script>