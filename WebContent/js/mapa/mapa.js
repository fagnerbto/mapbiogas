var map; 

var element = $('#popup');
var content = $('#popup-content');
var closer = $('#popup-closer');

var iconPequeno = context + '/img/verde.png'
var iconMedio = context + '/img/amarelo.png'
var iconGrande = context + '/img/azul.png'

var overlay = "";

var unidades = new Array();

var layerPequeno;
var layerMedio;
var layerGrande;

$(document).ready(function(){
	$("#myModal").on('shown.bs.modal', function(){
		$("#popup-closer").click();
		$("select option:selected").removeAttr("selected");
		$("#energia-termica").prop("checked",false); 
		$("#energia-eletrica").prop("checked",false);
		$("#energia-mecanica").prop("checked",false);
		$("#biometano").prop("checked",false);
	})
	$(".modal-wide").on("show.bs.modal", function() {
		var height = $(window).height();
		console.log(height);
		$(this).find(".modal-body").css("max-height", height);
	});

	var camadaBase1 = new ol.layer.Tile({
		id : "Camada Base - Ruas",
		title : "Camada Base - Ruas",
		camadaBase : true,
		source: new ol.source.BingMaps({
			key: 'AhEGPA8tz-KzW-PGgFwn-R4N1efXldwyKmjqmTvK0aXW1ZYcAxRjwMBggWsKFaKQ',
			imagerySet: 'road'
		})
	});
	
	var camadaBase2 = new ol.layer.Tile({
		title : "Camada Base - OSM",
		camadaBase : true,
		source: new ol.source.MapQuest({ layer: 'osm' })
	});

	var layerStm = new ol.layer.Group({
		layers: [camadaBase1],
		camadaBase : true,
		title: 'Camadas Bases'
	});
	
	
	
	overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
		element: element,
		autoPan: true,
		  autoPanAnimation: {
		    duration: 250
		  }
	}));

	map = new ol.Map({
		target: 'mapa',
		layers: [layerStm],
		view: 
			new ol.View({
				center: ol.proj.transform([-50.000, -18.000], 'EPSG:4326', 'EPSG:3857'),
		        zoom: 5
         	}),
         overlays: [overlay]         	
	});

	addMousePositionControl( 4, "EPSG:4326", document.getElementById("mousePosition") );

	addDivFerramenta({draw : false, modify : false, measure : true, raio : true, wms : true, alerta : true},  $("#ferramenta"));

	addLayerSwitch($("#layerSwitch"), true);

	getData();
});


function getData(){

	map.removeLayer(layerPequeno);
	map.removeLayer(layerMedio);		
	map.removeLayer(layerGrande);	

	$.get( context + "/getData/", {
		estadoId:             $("#estados > option:selected").val(), 
		municipioId:          $("#municipios  > option:selected").val(),
		situacaoDaPlantaId:   $("#situacao-da-planta > option:selected").val(),
		categoriaDeUnidadeId: null,
		fonteDoSubstratoId:   $("#fonte-de-substrato > option:selected").val(),
		escalaId:             $("#escalas > option:selected").val(), 
		porte:                $("#porte").val(),
		energiaTermica: $("#energia-termica").prop("checked"), 
		energiaEletrica: $("#energia-eletrica").prop("checked"),
		energiaMecanica: $("#energia-mecanica").prop("checked"),
		bioMetano:$("#biometano").prop("checked"),
		
	}
	, function( data ) {
		unidades = data;
		getFeatureMap(data);
		$("#myModal").modal('hide');
	});
	
}

function getFeatureMap(data){
	
	layerPequeno = new ol.layer.Group({ visibleLayerSwitch: true, title: "Pequeno Porte", icon: iconPequeno });
	layerMedio = new ol.layer.Group({ visibleLayerSwitch: true, title: "Médio Porte",icon: iconMedio });
	layerGrande = new ol.layer.Group({ visibleLayerSwitch: true, title: "Grande Porte", icon: iconGrande });
	
	$.each(data, function(i, item) {
		
		var iconFeature = new ol.Feature({
			geometry: new ol.geom.Point(ol.proj.transform([item.longitude,item.latitude], 'EPSG:4326', 'EPSG:3857')),
			id: i
		});
		
		iconFeature.setStyle(getIconFeature(item.escala.intervalo));
		
		var vectorSourceUnidade = new ol.source.Vector({
			visibleLayerSwitch: false,
			features: [iconFeature]
		});

		var vectorLayerUnidade = new ol.layer.Vector({
			visibleLayerSwitch: false,
			source: vectorSourceUnidade
		});
		
		switch(item.escala.intervalo) {	
	    case 1:
	    	layerPequeno.getLayers().push(vectorLayerUnidade);
	    	
	    	break;
	    case 2:
	    	layerMedio.getLayers().push(vectorLayerUnidade);
	    
	    	break;
	    case 3:
	    	layerGrande.getLayers().push(vectorLayerUnidade);
	    	
	    	break;
	    default:
	        {}
		}
		
	});
	
	map.addLayer(layerPequeno);
	map.addLayer(layerMedio);
	map.addLayer(layerGrande);

	addPopup();	

	atualizaLayerSwitch();
	
}

function getIconFeature(porte){
	switch(porte) {	
    case 1:
    	return new ol.style.Style({
    		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    			anchor: [0.5, 46],
    			anchorXUnits: 'fraction',
    			anchorYUnits: 'pixels',
    			opacity: 0.75,
    			offset:[-18,-9],
    			size: [66,66],    			
    			src: iconPequeno
    		}))
    	});        
    case 2:
    	return new ol.style.Style({
    		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    			anchor: [0.5, 46],
    			anchorXUnits: 'fraction',
    			anchorYUnits: 'pixels',
    			opacity: 0.75,
    			offset:[-18,-9],
    			size: [66,66],    			
    			src: iconMedio
    		}))
    	});        
    case 3:
    	return new ol.style.Style({
    		image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
    			anchor: [0.5, 46],
    			anchorXUnits: 'fraction',
    			anchorYUnits: 'pixels',
    			opacity: 0.75,
    			offset:[-18,-9],
    			size: [66,66],
    			src: iconGrande
    		}))
    	});
    default:
        {}
	}
	
}

function addPopup(){

	$('#popup-closer').click(function() {
		overlay.setPosition(undefined);
		$('#popup-closer').blur();
		return false;
	});
	
	map.on('click', function(evt) {
		
		var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature, layer) {
			return feature;
		});
		
		if (feature) {

			var geometry = feature.getGeometry();
			var coordinate = geometry.getCoordinates();
			var pan = ol.animation.pan({
			    duration: 350,
			    source: /** @type {ol.Coordinate} */ (map.getView().getCenter())
			  });
			map.beforeRender(pan);			
			map.getView().setCenter(coordinate);
			popupUnidade(feature.get('id'), coordinate);
		}
	});

}

function popupUnidade(id, coordinate){
	
	var json = unidades[id];
	
	if ( json != undefined ){		
		
		var html ='';
		html += '<table style="font-size: 12px;" class="table table-hover table-nao-pula-linha">';
		html += '	<thead>';
		html += '		<tr>';
		html += '			<th colspan="6"> <div class="text-center"> Codigo da unidade: ' + json.codigo + '</div> </th>';
		html += '		</tr>';
		html += '	</thead>';
		html += '	<tbody>'; 
		html += '		<tr>';
		html += '			<td> <div class="col-md-6 text-left"><b> Categoria da Unidade: </b></div> ';
		html += ' 			<div class="col-md-6 text-center"> ' + json.fonteDoSubstrato.categoriaDeUnidade.nome + ' </div> </td> ';		
		html += 		'</tr>'
		html += '		<tr>';
		html += '			<td> <div class="col-md-6 text-left"><b> Fonte do substrato: </b></div>';
		html += ' 			<div class="col-md-6 text-center"> ' + json.fonteDoSubstrato.nome + ' </div> </td>';		
		html += 		'</tr>'
		html += '		<tr>';
		
		var valorEstimado = "";
		if(json.valorEstimado == true){
			valorEstimado = '<h6 class="text-center"><span class="label label-warning">valor estimado</span></h6>';
		}
		html += '			<td > <div class="col-md-6 text-left"><b> Produção média diária de biogás: </b></div> '; 
		
		html += ' 			<div class="col-md-6 text-center"> ' + currencyFormatDE(json.producaoDiaria) + ' (m³/dia) '  + valorEstimado + '</div> </td> ';		
		html += '		</tr>'
		html += '		<tr>';
		html += '			<td > <div class="col-md-6 text-left"><b> Intervalo: </b></div> ';
		html += ' 			<div class="col-md-6 text-center"> ' + json.escala.legenda + ' </div> </td> ';		
		html += '		</tr>'			
		html += '		<tr>';
		html += '			<td > <div class="col-md-6 text-left"><b> Situação da planta: </b></div> ';
		html += ' 			<div class="col-md-6 text-center"> ' + json.situacaoDaPlanta.nome + '</div> </td> ';		
		html += '		</tr>'

		var porte = "";

		switch(json.escala.intervalo) {
	    case 1:
	    	porte = "Pequeno Porte";
	        break;
	    case 2:
	        porte = "Médio Porte";
	        break;
	    case 3:
	        porte = "Grande porte"
	        break;	        
	    default:
	        {}
		}		
		
		html += '		<tr>';
		html += '			<td > <div class="col-md-6 text-left"><b> Porte: </b></div> ';
		html += ' 			<div class="col-md-6 text-center"> ' + porte + '</div> </td> ';		
		html += '		</tr>'

			
		html += '		<tr>';
		html += '			<td colspan="6"> <div class="text-center"><b>Aplicação energética do biogás</></div> </td>';
		html += '		</tr>'

			
		var style = "";
		
		html += '		<tr> <td>';		
		if(json.biogasParaEnergiaTermica != true){
			style = "my-btn-default"
		}else {
			style ="btn-success"; 
		}
		html += '			 <button type="button" class="btn btn-xs ' + style + '">Energia Térmica </button>';

		if(json.biogasParaEnergiaEletrica != true){
			style = "my-btn-default"
		}else {
			style ="btn-success"; 
		}		
		html += '			<button type="button" class="btn btn-xs ' + style + '"> Energia Elétrica </button>';

		if(json.biogasParaEnergiaMecanica != true){
			style = "my-btn-default"
		}else {
			style ="btn-success"; 
		}
		
		html += '			<button type="button" class="btn btn-xs ' + style + '"> Energia Mecânica</button>';
		
		if(json.biogasParaBiometano != true){
			style = "my-btn-default"
		}else {
			style ="btn-success";
		}
		html += '		<button type="button" class="btn btn-xs ' + style + '">  Biometano/GNV </button>';
		html += 		'</tr>'			

		html += '</tbody>'
		html += '</table>'		
	}	
	
	$("#popup-content").html(html);

	overlay.setPosition(coordinate);
}

$("#button-search").click( function(){
	getData();
	$('#fecharmodal').click();
	
})

function currencyFormatDE (num) {
    return num
       .toFixed(0) // always two decimal digits
       .replace(".", ",") // replace decimal point character with ,
       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")// use . as a separator
}