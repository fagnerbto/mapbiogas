var popoverAtivo  = null;
var elementoClick = null;

$(document).ready(function() {


});

function addLayerSwitch(div, auto){

	var html = '';

	html += '<div id="conteiner-layerSwitch" title="Camadas">';
	html += '	<div id="layertree" class="tree"></div>';
	html += '	<hr style="margin-top: -10px; margin-bottom: 10px;">';
//	html += '	<div class="input-group">';
//	html += '		<input type="text" class="form-control" placeholder="Filtro..." id="filtrarLayerSwitch">';
//	html += '		<span class="input-group-addon" style="background: #337ab7; color: white;" id="executarFiltro">';
//	html += '			<span class="glyphicon glyphicon-search" style="width: 115px;" ></span>';
//	html += '		</span>';
//	html += '	</div>';
	html += '</div>';

	div.html(html);

	$("#conteiner-layerSwitch").dialog({ position : [150, 150], width : 250, autoOpen : auto, resizable: false });

	$("#executarFiltro").click(function(){

		atualizaLayerSwitch( $("#filtrarLayerSwitch").val() );
	
	});

	atualizaLayerSwitch();
	
}

function atualizaLayerSwitch(filtro){

	initializeTree(filtro);

	$('i').on('click', function() {
		
		if ( $(this).attr("class").search('glyphicon glyphicon-globe') < 0 ) {

			if ( $(this).attr("class").search('camadaBase') < 0 ) {

				var layername = $(this).data('layerid');
				var layer     = findBy( map.getLayerGroup(), 'title', layername );

				layer.setVisible(!layer.getVisible());

				if (layer.getVisible()) {
					$(this).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
				} 
				else {
					$(this).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
				}
			}
			else {

				var listaCamadaBase = $(".camadaBase");

				for (var i = 0; i < listaCamadaBase.length; i++) {

					var layername = listaCamadaBase[i].attributes.getNamedItem('data-layerid').value;
					var layer     = findBy(map.getLayerGroup(), 'title', layername);

					listaCamadaBase.removeClass('glyphicon-check').addClass('glyphicon-unchecked');
					layer.setVisible(false);

					if ( listaCamadaBase[i] === $(this)[0] ) {

						var layername = $(this).data('layerid');
						var layer     = findBy(map.getLayerGroup(), 'title', layername);

						layer.setVisible(true);

						try{
							$(this).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
						}catch (e) {}

						break;
					}
				}
			}
		}
	});

	$(".menuLayerSwitch").on('click', function(){

		if ( popoverAtivo !== null) {
			popoverAtivo.popover('hide');
			popoverAtivo = null;
		}

		if (elementoClick === null || elementoClick !== this.attributes.getNamedItem('data-layerNome').value) {
			popoverAtivo = $(this);
			$(this).popover('show');
			elementoClick = this.attributes.getNamedItem('data-layerNome').value;
		} else {
			elementoClick = null;
		}	

		$('.opacity').slider().on('slide', function(ev){ 

			$("#valorOpacity").text((ev.value).toFixed(1));
			var layername = $(this).data('layerid');
			var layer = findBy(map.getLayerGroup(), 'title', layername);
			layer.setOpacity(ev.value);

		});	

		$( ".popover" ).mouseleave(function() {
			$(this).popover('hide');
			popoverAtivo = null;
			elementoClick = null;
		});

	});
}

var selectCB = true;

var controler = 1;
var arrayControler = new Array();

function buildLayerTree(layer, filtro) {

	var elem, div = '', name = layer.get('title') ? layer.get('title') : "Camadas", icon = layer.get('icon') ? layer.get('icon') : null;
	
	controler ++;

	if ((filtro == null || filtro == "") || name.toLowerCase().indexOf(filtro.toLowerCase()) >= 0) {	

		if (layer.getLayers) {

			if ( (layer.get('camadaBase')) && ((layer.getLayers().getArray()).length != 1) ) {
				div += '<li> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-globe"> </i> <div class="inlineblock">' + name + '</div> </span>';
			}
			else {
				if (name == 'Camadas') {
					div += '<li> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-globe allCamadas"></i> <div class="inlineblock">' + name + '</div> </span>';
				} 
				else {
					if (name == 'Camadas Bases'){
						div += '<li> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-globe allCamadas"></i> <div class="inlineblock">' + name + '</div> </span>';
					}else{
					div += '<li> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-check" /> <img src="'+icon+'" style="width: 12px;"> <div class="inlineblock"> ' + name + '</div> </span>';	
					}
				}
			}
		}
		else {
			if ( layer.get('camadaBase') ) {
				if ( selectCB ) {
					div += '<li> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-check camadaBase"></i> <div class="inlineblock">' + name + '</div> </span>';
					layer.setVisible(selectCB);
					selectCB = false;
				}
				else {
					div += '<li> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-unchecked camadaBase"></i> <div class="inlineblock">' + name + '</div> </span>';
					layer.setVisible(selectCB);
				}
			}
			else {
				div += '<li style="display: none;"> <span> <i data-layerid="' + name + '" class="glyphicon glyphicon-check" /> <img src="'+icon+'" style="width: 12px;"> <div class="inlineblock">' + name + '</div> </span>';
			}		
		}
	}	

	if (layer.getLayers) {

		var sublayersElem = ''; 
		var layers = layer.getLayers().getArray();
		var len    = layers.length;

		controler = 1;
		
		arrayControler.push(len);

		for (var i = len - 1; i >= 0; i--) {
			if ( layers[i].get('visibleLayerSwitch') != false ){
				sublayersElem += buildLayerTree(layers[i], filtro);
			}
		}

		controler = 1;
		arrayControler = new Array();

		selectCB = true;
		
		elem = div + " <ul>" + sublayersElem + "</ul> </li>";

	} 
	else {
		elem = div + " </li>";
	}

	return elem;
}

function initializeTree(filtro) {

	var elem = buildLayerTree(map.getLayerGroup(), filtro);

	$('#layertree').empty().append(elem);

	$('.menuLayerSwitch').popover({html: true, trigger: "click", placement: 'right'});

	$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');

	$('.inlineblock').on('click', function(e) {
		
		var children = $(this).parent().parent().find('> ul > li');
	
		if (children.is(":visible")) {
			
			children.hide('fast');
			
			$(this).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');
		
		} 
		else {
			
			children.show('fast');
			
			$(this).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
		
		}

		e.stopPropagation();
		
	});	
}

function findBy(layer, key, value) {

	if (layer.get(key) === value) {
		
		return layer;
	
	}

	if (layer.getLayers) {

		var layers = layer.getLayers().getArray(),
		
		len = layers.length, result;

		for (var i = 0; i < len; i++) {

			result = findBy(layers[i], key, value);

			if (result) {
				
				return result;
			
			}
		}
	}
	
	return null;

}

function openLayerSwitch(){
	
	$("#conteiner-layerSwitch").dialog("open");

}