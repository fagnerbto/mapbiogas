<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Localhost-->
<%-- <c:set var="project" value="http://localhost:8888" /> --%>
<!-- Production-->
<c:set var="project" value="http://mapbiogas.cibiogas.org" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>${page}</title>
<script type="text/javascript">
	var context = '${project}';
</script>
<!-- Bootstrap core CSS -->
<link href="${project}/css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${project}/css/main.css" rel="stylesheet">
<link href="${project}/css/font-awesome.min.css" rel="stylesheet">

<script src="${project}/js/jquery.min.js"></script>
<script src="${project}/js/chart.js"></script>
</head>
<body>
<div class="container">
			<div class="row-fluid" style="margin-top: 100px !important">
				<div id="signupbox"
					class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2  mt40 mb30">
					<h3 class="text-danger text-center">${msg}</h3>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="panel-title">Login</div>
						</div>
						<div class="panel-body">
							<form role="form" id="formLogin" class="form-horizontal"
								action="${project}/login/loga/" method="POST">
								<div class="form-group">
									<label for="username" class="col-md-3 control-label">Username</label>
									<div class="col-md-9">
										<input id="name" type="text" class="form-control"
											name="user.username" placeholder="Username">
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="col-md-3 control-label">Password</label>
									<div class="col-md-9">
										<input id="name" type="password" class="form-control"
											name="user.password" placeholder="Password">
									</div>
								</div>
								
								<div class="form-group">
									<!-- Button -->
									<div class="col-md-offset-3 col-md-9">
										<button id="btn-signup" type="submit" class="btn btn-primary center-block">
											<i class="icon-hand-right"></i>Sign In
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
</div>
</body>
</html>