<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Localhost-->
<%-- <c:set var="project" value="http://localhost:8888" /> --%>
<!-- Production-->
<c:set var="project" value="http://mapbiogas.cibiogas.org" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>${page}</title>
<!-- Bootstrap core CSS -->
<link href="${project}/css/bootstrap.css" rel="stylesheet">
<script type="text/javascript">
	var context = '${project}';
</script>
<!-- Custom styles for this template -->
<%-- <link href="${project}/css/main.css" rel="stylesheet"> --%>
<link href="${project}/css/font-awesome.min.css" rel="stylesheet">
<script src="${project}/js/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="${project}/js/chart.js"></script>
<script src="${project}/js/bootstrap.js"></script>

<link rel="stylesheet" type="text/css" href="http://openlayers.org/en/v3.0.0/css/ol.css">
<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${project}/OpenLayers3/css/style.css">
<link rel="stylesheet" type="text/css" href="${project}/css/map.css">

</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top" >
		<div class="row">
			<div class="col-lg-2 col-lg-offset-4 col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 col-xs-5">
				<a id="button-show-modal" href="#myModal" style="width:200px; margin-top:10px" class="btn btn-primary btn-sm " data-toggle="modal">Busca</a>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-5 col-xs-offset-1 col-xs-5">
				<a id="button-show-contact" href="#contact" style="margin-top:10px; " class="btn btn-primary btn-sm " data-toggle="modal">Contato</a>
				<a id="button-show-about" href="#about" style="margin-top:10px; " class="btn btn-primary btn-sm " data-toggle="modal">Sobre</a>
				<c:if test="${not empty mensagem}">
					<button type="button" style="margin-top:10px;"  class="btn btn-success btn-sm pull-right">${mensagem}</button>
				</c:if>										
			</div>
			</div>
	</div>

	<section style="min-height:500px !important;">
			<div id="mapa">
				<div id="popup" class="ol-popup">
	                <a href="#" id="popup-closer" class="ol-popup-closer"></a>
	                <div id="popup-content">
			        </div>	             
	           	</div>
			</div>
			
			<div id="mousePosition"></div>
			<div id="ferramenta"   ></div>
			<div id="layerSwitch"  ></div>
	</section>
	
<div id="footer">
	<div id="footerMenu">
		<img src="${project}/img/logos/barra.png" alt="" class="img-responsive" />
	</div>
</div>
	<div id="contact" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">CONTATO</h4>
				</div>
				<div class="modal-body">
				<form role="form" action="${project}/contato/" method="post"  accept-charset="UTF-8">
				        <label for="InputName">Nome</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nome" id="InputName" placeholder="Nome" required>
							<span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
						</div>
						<div class="form-group">
				        <label for="InputEmail">Email de contato</label>
				        <div class="input-group">
				          <input type="email" class="form-control" id="InputEmail" name="email" placeholder="Enter Email" required  >
				          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
				         </div>
						</div>
				      <div class="form-group">
				        <label for="InputMessage">Deixe sua mensagem</label>
				        <div class="input-group">
				          <textarea name="mensagem" id="InputMessage" class="form-control" rows="5" required></textarea>
				          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
						</div>
						<button id="fecharmodal" type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success pull-right ">
				      </div>
				  </form>
				</div>
			</div>
		</div>
	</div>
	<div id="about" class="modal modal-wide fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Sobre</h4>
				</div>
				<div class="modal-body">
				
				<h3>A INICIATIVA</h3>

					<p class="text-justify">
						O <strong>BiogasMap</strong> é uma ferramenta web que possibilita
						a visualização das unidades de produção e uso energético de biogás
						do Brasil em um mapa dinâmico público e online. O <strong>BiogasMap</strong>
						vem para preencher uma lacuna do setor no sentido de se conhecer o
						potencial de biogás já realizado e implantado no Brasil.
					</p>

					<p class="text-justify">
						Esta ferramenta foi desenvolvida pelo <a
							href="http://cibiogas.org" target="_blank"><strong>CIBiogásER
								- Centro Internacional de Energias Renováveis – Biogás </strong></a> e pelo <a
							href="http://hidroinformatica.org" target="_blank"><strong>CIH
								- Centro Internacional de Hidroinformática </strong></a> com o apoio e
						financiamento do <a
							href="http://www.cidades.gov.br/saneamento-cidades/probiogas"
							target="_blank"><strong>PROBIOGÁS - Projeto
								Brasil-Alemanha de Fomento ao Aproveitamento Energético de
								Biogás no Brasil</strong></a>. O PROBIOGAS é fruto da cooperação técnica
						entre o Governo Brasileiro, por meio da Secretaria Nacional de
						Saneamento Ambiental do Ministério das Cidades, e o Governo
						Alemão, por meio da Deutsche Gesellschaft für Internationale
						Zusammenarbeit (GIZ) GmbH.
					</p>

					<p class="text-justify">
						O levantamento dos dados das unidades ocorreu no âmbito de um projeto de desenvolvimento tecnológico e inovação com recursos financeiros do CIBiogas e apoio técnico de uma aluna de doutorado do <a
							href="http://www.fem.unicamp.br/~hpcpg/mostrar.php?n=index&dep=planejamento_sistemas_energeticos"
							target="_blank"><strong>Programa de Pós-graduação em
								Planejamento de Sistemas Energéticos</strong></a> da Universidade Estadual de
								Campinas.
					</p>

					<p class="text-justify">O BiogasMap faz parte do Cadastro
						Nacional do Biogás que está sendo criado pelo CIBiogas, uma
						iniciativa mais ampla que visa compilar e disponibilizar dados
						sobre o setor de biogás de forma colaborativa, imparcial e
						transparente. O objetivo é incentivar o desenvolvimento do setor
						de biogás brasileiro por meio da disseminação de informações sobre
						potencial de produção, unidades de produção existentes,
						tecnologias disponíveis, projetos de pesquisas e desenvolvimento,
						linhas de financiamento, legislação e regulamentação, consultores,
						fornecedores.</p>
					<p class="text-justify">É importante destacar que a perspectiva
						é que os dados sejam atualizados anualmente pelo CIBiogas e seus
						parceiros e que a ferramenta seja melhorada e ampliada conforme a
						demanda do setor.</p>

					<h3>O BIOGASMAP</h3>

					<p class="text-justify">A arquitetura do BiogasMap permite a
						apresentação dos dados das unidades de produção e uso energético
						de biogás de forma organizada e a pesquisa com filtros de
						indicadores. É possível, ainda, clicando sobre o ícone da unidade
						no mapa, visualizar um resumo dos dados, como: categoria, fonte de
						substrato, uso energético do biogás na unidade, produção média
						diária de biogás e porte da unidade.</p>

					<p class="text-justify">No mapa é possível aproximar e afastar
						a imagem, tendo uma visão mais detalhada ou mais geral do
						cadastro. O mapa utilizado pode ser o mapa político ou o mosaico
						de imagens de satélite, ambos da ferramenta gratuita Bing da
						Microsoft.</p>
<h3>A METODOLOGIA</h3>

<p class="text-justify">O levantamento de dados foi realizado entre janeiro e novembro de 2015, partindo-se de sites de busca, bancos de dados do setor energético e ambiental, empresas prestadores de serviço, pesquisadores e especialistas do setor. Após a coleta inicial de dados, foi realizado o contato com o responsável pela unidade de produção e uso energético de biogás para coletar e confirmar os dados do cadastro. A localização geográfica das unidades foi confirmada e ajustada utilizando-se o aplicativo GoogleMaps.</p>

<p class="text-justify">Para o caso de unidades que não tinham conhecimento da sua produção média de biogás, foi utilizada uma metodologia do CIBiogas para estimar a produção a partir da quantidade de animais, no caso da agropecuária, considerando que apenas 70% do potencial teórico é convertido e potencial técnico de biogás. Outro meio de estimar-se a produção média diária de biogás foi por meio do consumo médio de biogás dos grupos motogeradores. Destaca-se que, pela falta de dados detalhados da composição do biogás de cada unidade, essa informação não é disponibilizada no BiogasMap.</p>

<p class="text-justify">A partir dos dados de produção de biogás as unidades foram classificadas segundo o porte entre pequena, média e grande e também em 7 intervalos quantitativos ( &lt; 1.250 m3/dia - Porte 1; 1.251 a 2.500 m3/dia - Porte 2; 2.501 a 8.500 m3/dia - Porte 3; 8.501 a 12.500 m3/dia - Porte 4; 12.501 a 85.500 m3/dia - Porte 5; 85.501 a 350.000 m3/dia - Porte 6; &gt;  350.000 m3/dia - Porte 7). Essa categorização foi adaptada de referências de regulamentações do setor de biogás da Alemanha.</p>

<p class="text-justify">As unidades também foram categorizadas em 5 classes, conforme o tipo de instalação que produz o substrato para a biodigestão e conforme a própria fonte de substrato, sendo: Indústria (Indústria sucroenergética, Abatedouro de aves ou suínos, Indústria de lacticínios e Indústria de alimentos e/ou bebidas); Agropecuária (Suinocultura; Avicultura de postura ou corte e Bovinocultura de leite ou corte); Aterro sanitário (Aterro sanitário); Estação de tratamento de esgoto (Esgoto e Lodo de esgoto); Codigestão (Codigestão de resíduos e efluentes). Essa classificação foi baseada na utilizada pelo Grupo de Trabalho 37 – Biogás da Agência Internacional de Energia nos relatórios anuais dos países sobre biogás.</p>

<h3>CONSIDERAÇÕES</h3>
<p class="text-justify">
Os dados desse cadastro representam valores aproximados com o objetivo de dar uma visão geral do setor de biogás do Brasil e não devem ser aplicados à elaboração de projetos técnicos, análises econômicas ou planejamentos detalhados.
</p>
<p class="text-justify">
Além disso, por ser uma iniciativa pioneira, podem haver unidades de produção de biogás com uso energético que ainda não estejam cadastradas no BiogasMap. Assim, qualquer responsável por unidade que queira incluir ou corrigir dados de uma unidade, poderá solicitá-lo ao CIBiogás. Além disso, informações e sugestões de todos os atores do setor de biogás também poderão ser enviadas ao CIBiogas.
</p>
<p class="text-justify">
O contato pode ser feito pela ferramenta de mensagem disponível clicando no botão <strong>Contato</strong> presente na tela de mapa ou diretamente pelo e-mail <strong>cadastro.biogas@cibiogas.org</strong>.
</p>
				</div>
				<div class="modal-footer">
				
				<button id="fecharmodal" type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Selecione os filtros desejados</h4>
                </div>
                <div class="modal-body">
                	<div class="row">
						<h5 ><b>Filtros Básicos</b></h5>
						
						<div class="col-md-12 mt15">
							<select id="situacao-da-planta" name="unidade.situacaoDaPlanta.id" class="form-control">
								<option value="">Situação da Unidade</option>
								<c:forEach items="${situacoes}" var="situacao">
									<option value="${situacao.id}">${situacao.nome}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-md-6 mt15">
							<select id="escalas" name="unidade.escala.id"
								class="form-control">
								<option value="">Categoria</option>
								<c:forEach items="${escalas}" var="escala"
	 									varStatus="contador">  
									<option value="${escala.id}">${escala.legenda}</option> 
 								</c:forEach> 
							</select>
						</div>
						<div class="col-md-6 mt15">
							<select id="fonte-de-substrato" name="unidade.fonteDoSubstrato.id" class="form-control" required>
								<option value="">Fonte do Substrato</option>
								<c:forEach items="${fontes}" var="fonte"
									varStatus="contador">
									<option value="${fonte.id}">${fonte.nome}</option>
								</c:forEach>
								
							</select>
						</div>
					</div>
					<div class="row mt15">	
						<h5 ><b>Aplicação do Biogás</b></h5>					
						<div class="col-md-6 text-left">
						<input type="checkbox" name="energiaTermica" id="energia-termica" >&nbsp;<label for="energia-termica" >Energia Térmica</label>
						</div>
						<div class="col-md-6 text-left">
						 <input type="checkbox" name="energiaEletrica" id="energia-eletrica" >&nbsp;<label for="energia-eletrica" > Energia Elétrica</label>
						</div>
						<div class="col-md-6 text-left">
						<input type="checkbox" name="energiaMecanica" id="energia-mecanica" >&nbsp;<label for="energia-mecanica" > Energia Mecânica</label>						
						</div>
						<div class="col-md-6 text-left">
						<input type="checkbox" name="bioMetano" id="biometano" >&nbsp;<label for="biometano" > GNR/Biometano</label>
						</div>
					</div>


					<div class="row mt15">
					<h5><b>Filtros por Localização</b></h5>

					<div class="col-md-6 mt15">
						<select id="estados" name="estado-id" class="form-control">
							<option value="">Selecione o Estado:</option>
							<c:forEach items="${estados}" var="estado"
								varStatus="contador">
								<option value="${estado.id}">${estado.nome}</option>
							</c:forEach>
						</select> 
					</div>
					<div class="col-md-6 mt15">
						<select id="municipios" name="unidade.municipio.id" class="form-control" required>
							<option value="">Selecione o Municipio:</option>
						</select>
					</div>                	
                	</div>                
                </div>
                <div class="modal-footer">
                    <button id="fecharmodal" type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button"  class="btn btn-primary " id="button-search" >Buscar</button>                    
                </div>
            </div>
        </div>
    </div>

	
	
<script type="text/javascript" src="http://openlayers.org/en/v3.0.0/build/ol.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.6/proj4.js"></script>

<script src="http://epsg.io/29191.js"  type="text/javascript"></script>
<script src="http://epsg.io/31981.js"  type="text/javascript"></script>
<script src="http://epsg.io/900913.js" type="text/javascript"></script>

<!-- Mapa -->
<script type="text/javascript" src="${project}/js/mapa/mapa.js"></script>
<!-- Controles para o mapa -->
<script type="text/javascript" src="${project}/OpenLayers3/js/controle/zoomslider.js"></script>
<script type="text/javascript" src="${project}/OpenLayers3/js/controle/scaleline.js"></script>
<script type="text/javascript" src="${project}/OpenLayers3/js/controle/mouseposition.js"></script>
		
<%-- Ferramentas --%>
<script type="text/javascript" src="${project}/OpenLayers3/js/ferramenta/create.js"></script>
<script type="text/javascript" src="${project}/OpenLayers3/js/ferramenta/ferramenta.js"></script>
<script type="text/javascript" src="${project}/OpenLayers3/js/ferramenta/layerswitch.js"></script>
<script type="text/javascript" src="${project}/OpenLayers3/js/ferramenta/novostyle.js"></script>



<script type="text/javascript">
var result = "";
 
	$("#estados").change(
					function() {
						var estadoId = $("#estados option:selected").val();
						$.getJSON( context + 
										"/municipios/estado/"
												+ estadoId,
										function(data) {
											$("#municipios").html("");
											var optionMunicipios = ""
											optionMunicipios += '<option value="0">Selecione</option> \n';
											$
													.each(
															data,
															function(key, val) {
																optionMunicipios += '<option id="municipio-' + val.id + '" value= "' + val.id + '">'
																		+ val.nome
																		+ '</option> \n';
															});
											$("#municipios").append(
													optionMunicipios);
										})
					})
		</script>
	
	
</body>
</html>