<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Localhost-->
<%-- <c:set var="project" value="http://localhost:8888" /> --%>
<!-- Production-->
<c:set var="project" value="http://mapbiogas.cibiogas.org" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>${page}</title>
<script type="text/javascript">
	var context = '${project}';
</script>
<!-- Bootstrap core CSS -->
<link href="${project}/css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${project}/css/main.css" rel="stylesheet">
<link href="${project}/css/font-awesome.min.css" rel="stylesheet">

<script src="${project}/js/jquery.min.js"></script>
<script src="${project}/js/chart.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="${project}/js/bootstrap.js"></script>


</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left">
					<li class="">
						<a href="${project}/admin/lista/">Listar Unidades</a>
					</li>
					<li class="">
						<a href="${project}/admin/form/">Cadastrar nova Unidade</a>
					</li>					
					<li class="">
						<a href="${project}/map">Ir para o Mapa</a>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="${project}/logout/">Sair</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<div class="container" style="margin-top: 100px">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">Localização</h3>
			</div>
			<div class="panel-body">
				<c:forEach items="${lista}" var="unidade">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2">
									<strong>Código:</strong>
								</div>
								<div class="col-md-2">${unidade.codigo}</div>
								<div class="col-md-2">
									<strong>Cidade:</strong>
								</div>
								<div class="col-md-2">${unidade.municipio.nome}</div>
								<div class="col-md-2">
									<strong>Estado:</strong>
								</div>
								<div class="col-md-2">${unidade.municipio.estado.nome}</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2">
									<strong>SituacaoDaPlanta:</strong>
								</div>
								<div class="col-md-2">${unidade.situacaoDaPlanta.nome}</div>
								<div class="col-md-2">
									<strong>FonteDoSubstrato:</strong>
								</div>
								<div class="col-md-2">${unidade.fonteDoSubstrato.nome}</div>
								<div class="col-md-2">
									<strong>Categoria:</strong>
								</div>
								<div class="col-md-2">
									${unidade.fonteDoSubstrato.categoriaDeUnidade.nome}</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2">
									<strong>ProducaoDiaria: <small>m³</small>
									</strong>
								</div>
								<div class="col-md-2">${unidade.producaoDiaria}</div>
								<div class="col-md-2">
									<strong>Escala de produção:</strong>
								</div>
								<div class="col-md-2">${unidade.escala.legenda}</div>
								<div class="col-md-2">
									<strong>Porte:</strong>
								</div>
								<div class="col-md-2">${unidade.escala.porte}</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">

								<div class="col-md-2">
									<strong>Intervalo:</strong>
								</div>
								<div class="col-md-2">${unidade.escala.intervalo}</div>
							</div>
						</div>



						<div class="col-md-12">
							<a class="btn-sm btn-warning"
								href="${project}/unidade/edit/${unidade.id}">Alterar</a>
							<button class="btn-sm btn-danger glyphicon glyphicon-trash modalExcluir"								 
								id="${unidade.id}" 
								data-titulo="Código da Unidade: ${unidade.codigo}"
								data-mensagem="Você deseja realmente excluir o registro?"
								data-excluir="excluir"
								>Excluir</button>
						</div>
						<!-- 			<div class="col-md-2"><strong>Valor:</strong></div> -->
						<%-- 			<div class="col-md-2">${unidade.escala.valor}</div> --%>
					</div>
					<hr />
				</c:forEach>
			</div>
			<a class="btn btn-success pull-right"
				href="${linkTo[UnidadeController].map}">Ir para o Mapa</a>
		</div>
	</div>
	
	<jsp:include page="../includes/modalExcluir.jsp"></jsp:include>
	
		<script type="text/javascript">
		
		$(".modalExcluir").on('click', function(){
	   		console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
	   		$('#modalExcluirTitulo')  .text(this.attributes.getNamedItem('data-titulo').value);
	 		$('#modalExcluirMensagem').text(this.attributes.getNamedItem('data-mensagem').value);
	 		$('#excluirConfirmar')    .html('<a class="btn btn-info" href="${project}/unidade/exclude/' + this.attributes.getNamedItem('id').value + '">'+ this.attributes.getNamedItem('data-excluir').value +'</a> ');	 		

	   		
	   		$('#myModalExcluir').modal('show');
	   	});
	    
	</script>
</body>
</html>