<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Localhost-->
<%-- <c:set var="project" value="http://localhost:8888" /> --%>
<!-- Production-->
<c:set var="project" value="http://mapbiogas.cibiogas.org" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>${page}</title>
<!-- Bootstrap core CSS -->
<link href="${project}/css/bootstrap.css" rel="stylesheet">
<script type="text/javascript">
	var context = '${project}';
</script>
<!-- Custom styles for this template -->
<link href="${project}/css/main.css" rel="stylesheet">
<link href="${project}/css/font-awesome.min.css" rel="stylesheet">

<script src="${project}/js/jquery.min.js"></script>
<script src="${project}/js/chart.js"></script>
</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left">
					<li class="">
						<a href="${project}/admin/lista/">Listar Unidades</a>
					</li>
					<li class="">
						<a href="${project}/admin/form/">Cadastrar nova Unidade</a>
					</li>					
					<li class="">
						<a href="${project}/map">Ir para o Mapa</a>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="${project}/logout/">Sair</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<div class="container" style="margin-top: 100px">
		<form action="${project}/unidade/salvar" method="POST"
			class="form-signin">
			<fieldset>
				<!-- Form Name -->
				<legend>Cadastro de Unidade</legend>
				<div class="row">
					<div class="col-md-4">
						<!-- Select Basic -->
						<div class="form-group col-md-12">
							<label class=" control-label" for="situacao-da-planta">Situação da planta</label>
							<div class="">
								<select id="situacao-da-planta" name="unidade.situacaoDaPlanta.id"
									class="form-control">
									<c:forEach items="${situacoes}" var="situacao">
										<option value="${situacao.id}">${situacao.nome}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<!-- Select Basic -->
						<div class="form-group col-md-12">
							<label class="control-label" for="producao-diaria">Produção média diária de biogás <small>(m³)</small></label>
							<input id="producao-diaria" name="unidade.producaoDiaria" placeholder="" min="0" step="1" class="form-control input-md"required type="number" value="">
						</div>
						<div class="form-group col-md-12" >
							<label class="col-md-9 control-label" for="">Este é um valor estimado?</label>
							<label class="col-md-2 control-label" for="estimado">Sim</label>
							<input class="col-md-1" type="checkbox" name="unidade.valorEstimado" id="estimado" value="true">													    
						</div>
					</div>
					
				<div class="col-md-4">
					<!-- Select Basic -->
						<div class="form-group col-md-12">
							<label class="control-label" for="categorias">Selecione a
								Categoria:</label> 
								<select id="categorias" name="categoria-id"
								class="form-control">
								<option value="0">Selecione</option>
								<c:forEach items="${categorias}" var="categoria"
									varStatus="contador">
									<option value="${categoria.id}">${categoria.nome}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="fonte-de-substrato">Selecione
								a fonte do substrato:</label> 
								<select id="fonte-de-substrato"
								name="unidade.fonteDoSubstrato.id" class="form-control" required>
								<option value="0">Selecione</option>
							</select>

						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="codigo">Código de controle</label>
							<input id="codigo" name="unidade.codigo" placeholder="" min="0" step="1" class="form-control input-md" required type="number" value="">
						</div>						
					</div>								
				<!-- Multiple Checkboxes -->
				<div class="form-group col-md-4">
					<label class="control-label col-md-12" for="checkboxes">Aplicação
						do Biogás</label>
					<div class="">
						<div class="radio ">
							<label for="radios-0" class="col-md-6"> Energia Térmica </label>
							<label for="radios-0s" class="col-md-3"> Sim
							<input type="radio" name="unidade.biogasParaEnergiaTermica" id="radios-0s" value="true">
							</label>
							<label for="radios-0n" class="col-md-3"> Não
							<input type="radio" name="unidade.biogasParaEnergiaTermica" id="radios-0n" value="false" checked>
							</label>							
						</div>
						<hr />
						<div class="radio ">
							<label for="radios-1" class="col-md-6"> Energia Elétrica </label>
							<label for="radios-1s" class="col-md-3"> Sim
							<input type="radio" name="unidade.biogasParaEnergiaEletrica" id="radios-1s" value="true">
							</label>
							<label for="radios-1n" class="col-md-3"> Não
							<input type="radio" name="unidade.biogasParaEnergiaEletrica" id="radios-1n" value="false" checked>
							</label>							
						</div>
						<hr />
						<div class="radio ">
							<label for="radios-2" class="col-md-6"> Energia Mecânica </label>
							<label for="radios-2s" class="col-md-3"> Sim
							<input type="radio" name="unidade.biogasParaEnergiaMecanica" id="radios-2s" value="true">
							</label>
							<label for="radios-2n" class="col-md-3"> Não
							<input type="radio" name="unidade.biogasParaEnergiaMecanica" id="radios-2n" value="false" checked>
							</label>							
						</div>
						<hr />
						<div class="radio ">
							<label for="radios-3" class="col-md-6">GNR/Biometano</label>
							<label for="radios-3s" class="col-md-3"> Sim
							<input type="radio" name="unidade.biogasParaBiometano" id="radios-3s" value="true">
							</label>
							<label for="radios-3n" class="col-md-3"> Não
							<input type="radio" name="unidade.biogasParaBiometano" id="radios-3n" value="false" checked>
							</label>							
						</div>
						<hr />
					</div>
				</div>
				</div>
				<div class="row" style="padding-top:15px">
					<div class="col-md-8" id="map" style="height: 420px;">
					</div>
					<div class="col-md-4">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Localização</h3>
								</div>
								<div class="panel-body">
									<label class="control-label" for="estados">Selecione
										o Estado:</label> <select id="estados" name="estado-id"
										class="form-control">
										<option value="0">Selecione</option>
										<c:forEach items="${estados}" var="estado"
											varStatus="contador">
											<option value="${estado.id}">${estado.nome}</option>
										</c:forEach>
									</select> <label class="control-label" for="municipios">Selecione
										o Municipio:</label> <select id="municipios" 
										name="unidade.municipio.id"
										class="form-control" required>
										<option value="0">Selecione</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group col-md-12 mt15">
									<div class="panel panel-success">
										<div class="panel-heading">
											<h3 class="panel-title">Coordenadas</h3>
										</div>
										<div class="panel-body">

											<label class="control-label" for="latitude">Latitude</label>
											<div class="">
												<input id="latitude" name="unidade.latitude" type="text"
													placeholder="-15.794229"
													class="form-control input-md coordinate" required
													value="-15.794229">
											</div>
										</div>
										<!-- Text input-->
										<div class="form-group col-md-12">
											<label class="control-label" for="longitude">Longitude</label>
											<div class="">
												<input id="longitude" name="unidade.longitude" type="text"
													placeholder="-47.882166"
													class="form-control input-md coordinate" required
													value="-47.882166">
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

				<div class="row">
				<!-- Text input-->
				</div>
				<!-- Button (Double) -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="salvar-unidade"></label>
					<div class="col-md-8">
						<button id="salvar-unidade" name="salvar-unidade"
							class="btn btn-success">Salvar</button>
						<button id="limpar-unidade" name="limpar-unidade"
							class="btn btn-inverse">Voltar</button>
					</div>
				</div>
			</fieldset>

		</form>
	</div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="${project}/js/bootstrap.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	
	
  <script>	
       function init_map() {
  			
    	var location = new google.maps.LatLng(-15.794229,-47.882166);

    	var map;
	    
   	    var args = {
   	            center: location,
   	            backgroundColor: '#ffffff',
   	            zoom: 4,
   	            disableDefaultUI: true,
   	            zoomControl: false,
   	            disableDoubleClickZoom: true,
   	            panControl: false,
   	            mapTypeControl: false,
   	            scaleControl: false,
   	            scrollwheel: false,
   	            streetViewControl: false,
   	            draggable: true,
   	            overviewMapControl: false,
   	            mapTypeId: google.maps.MapTypeId.ROADMAP,

		};
		
   	    map = new google.maps.Map(document.getElementById('map'),args);        
	
 
	    var marker = new google.maps.Marker({
	    	position: location,
	    	map: map,
	    	draggable:true
	    });

	    map.addListener('click', function(e){
				marker.setPosition(e.latLng);
		    	$("#latitude").val(e.latLng.lat());
		    	$("#longitude").val(e.latLng.lng());

		});
		
	    marker.addListener('dragend', function(e){
// 	    	console.log("Latitude: " + e.latLng.lat() + " Longitude: " + e.latLng.lng());
	    	$("#latitude").val(e.latLng.lat());
	    	$("#longitude").val(e.latLng.lng());
	    })

	    $(".coordinate").blur(function(){
	    	var lat = parseFloat($("#latitude").val());
	    	var lng = parseFloat($("#longitude").val());
	    	
	    	
	    	var newLatLng = new google.maps.LatLng(lat, lng);
	    	
	    	map.setZoom(11);
	    	map.setCenter(newLatLng);
	    	marker.setPosition(newLatLng);
	    })
     
     	$("#estados").change(
     		function(){     			
     	     	var estadoId =  $("#estados option:selected").val();

     	     	$.getJSON(context + "/municipios/estado/"+estadoId, function(data) {
     				$("#municipios").html("");
     				var optionMunicipios = ""
     					optionMunicipios += '<option value="0">Selecione</option> \n';
     				$.each( data, function( key, val ) {
     					optionMunicipios += '<option id="municipio-' + val.id + '" value= "' + val.id + '">' + val.nome + '</option> \n'; 
     				});
     				$("#municipios").append(optionMunicipios);
     			})
     		})
     		
     	$("#municipios").change(function(){
     		var geocoder = new google.maps.Geocoder();     		
      		geocoder.geocode({'address': $("#municipios option:selected").html() + " state of " + $("#estados option:selected").html() }, function(results, status) {
     			 if (status === google.maps.GeocoderStatus.OK) {
      				map.setZoom(13);
     				map.setCenter(results[0].geometry.location);
     				marker.setPosition(results[0].geometry.location);
     				$("#latitude").val(results[0].geometry.location.lat)
     				$("#longitude").val(results[0].geometry.location.lng)
     			 }else {
     				geocoder.geocode({'address': " state: " + $("#estados option:selected").html() }, function(results, status) {
     	     			 if (status === google.maps.GeocoderStatus.OK) {
     	      				map.setZoom(4);
     	     				map.setCenter(results[0].geometry.location);
     	     				marker.setPosition(results[0].geometry.location);
     	     				 
     	     			 }else{
     	     				alert('Geocode was not successful for the following reason: ' + status);
     	     			 }
     			 	})	
     			 }
     		})     		
     	})
      }
 
      google.maps.event.addDomListener(window, 'load', init_map);
     </script>
     
     <script type="text/javascript">
     	$("#categorias").change(
     		function(){
     			var categoriaId = $("#categorias option:selected").val();
     			$.getJSON(context + "/fonteDoSubstratoPorCategoriaDeUnidade/"+ categoriaId, function(data){
     				$("#fonte-de-substrato").html("");
     				var optionFontes = "";
					    optionFontes += '<option value="0">Selecione</option> \n';
					
					$.each(data, function(key,val) {
						optionFontes += '<option id= "fonte-' + val.id + '" value="' + val.id + '">' + val.nome + '</option> \n'; 						
					});
					console.table(data)
					console.log(optionFontes)
     				$("#fonte-de-substrato").append(optionFontes);
     			})
     	})    
     </script>
</body>
</html>