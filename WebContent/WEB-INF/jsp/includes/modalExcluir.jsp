<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal" id="myModalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				
		  		<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				
				<h4 class="modal-title" id="modalExcluirTitulo"></h4>
			
			</div>
			
			<div class="modal-body">
				
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="text-center" style="margin-bottom: 0px;">							
							<h4 id="modalExcluirMensagem"></h4>
						</div>
					</div>
				</div>
			
			</div>
			
			<div class="modal-footer">
				<div style="float: right;" id="excluirConfirmar"></div>
				<div style="float: right; margin-right: 10px">
					<button type="button" class="btn btn-info-cic-plata" data-dismiss="modal">Cancelar</button>
    	   		</div>
			</div>
		</div>
	</div>
</div>